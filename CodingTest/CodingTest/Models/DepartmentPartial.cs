﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CodingTest.Models
{
    public class DepartmentPartial
    {

    }

    [MetadataType(typeof(Department_Meta))]
    public partial class Department
    {

        private class Department_Meta
        {
            [Required(ErrorMessage = "Name Field is required")]
            public string Name { get; set; }

            [Required]
            public bool Active { get; set; }

        }
    }

}
