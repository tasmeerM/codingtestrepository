﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CodingTest.Models
{
    public class UserPartial
    {

    }

    [MetadataType(typeof(User_MetaData))]
    public partial class User
    {
        private class User_MetaData
        {
            [Required(ErrorMessage = "First Name Field is required")]
            public string FirstName { get; set; }
            [Required(ErrorMessage = "Last Name is required")]
            public string LastName { get; set; }
            [Required(ErrorMessage = "Country Name is required")]
            public string Country { get; set; }
            [Required]
            public bool Active { get; set; }

        }
    }
}