﻿using CodingTest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CodingTest.Services
{
    public class DepartmentService
    {
        private readonly CodingTestDBEntities _db;

        public DepartmentService(CodingTestDBEntities context)
        {

            _db = context;
            
        }
    
        public DepartmentService() : this(new CodingTestDBEntities())
        {

        }

        public List<Department> GetAllDepth()
        {
            return _db.Departments.ToList();
        }

        public Department FindDepartment(int? id)
        {
            return _db.Departments.Find(id);
        }

        public void createDept(Department dept)
        {
            _db.Departments.Add(dept);
            _db.SaveChanges();
        }

        public void DeleteConfirmed(int id )
        {
            Department department = _db.Departments.Find(id);
            _db.Departments.Remove(department);
            _db.SaveChanges();
        }

        public void editDepartment(Department dept)
        {
            _db.Entry(dept).State = EntityState.Modified;
            _db.SaveChanges();
        }



        public List<User> GetUserByDepartmentId(int id)
        {
            return _db.Users.Where(t=>t.DepartmentId==id).Include(t=>t.Department).ToList();
        }
        


    }
}