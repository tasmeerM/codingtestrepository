﻿using CodingTest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CodingTest.Services
{
    public class UserService
    {
        private readonly CodingTestDBEntities _db;

        public UserService(CodingTestDBEntities context)
        {

            _db = context;

        }

        public UserService() : this(new CodingTestDBEntities())
        {

        }

        

        public List<User> GetAllUsers()
        {
            return _db.Users.ToList();
        }

        public User FindUser(int? id)
        {
            return _db.Users.Find(id);
        }

        public void createUser(User usr)
        {
            _db.Users.Add(usr);
            _db.SaveChanges();
        }


        public void editUser(User usr)
        {
            _db.Entry(usr).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public List<Department> GetAllDepth()
        {
            return _db.Departments.ToList();
        }

        public void DeleteConfirmed(int id)
        {
            User user = _db.Users.Find(id);
            _db.Users.Remove(user);
            _db.SaveChanges();
        }


    }
}